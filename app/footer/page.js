'use client'
import React from 'react'
import {  FaLinkedin,FaGitlab,FaInstagram  } from 'react-icons/fa'
import "./Footer.css";
function Footer() {
  return (
    <div className='social'>
       <ul>
       <li><a href="https://www.instagram.com/massinissa_bsa/"><FaInstagram size={25}></FaInstagram></a></li>
          <li><a href="https://gitlab.com/group-test95/massinissabessa_portfalio.git"><FaGitlab size={25}></FaGitlab></a></li>
          <li><a href="https://www.linkedin.com/in/massinissa-bessa-95b33a27b/"><FaLinkedin size={25}></FaLinkedin></a></li>
        </ul>
    </div>
  )
}

export default Footer