'use client'
import axios from 'axios';
import React, { useEffect, useState } from 'react';
//import { useNavigate, useParams } from 'react-router-dom';
import "./AjouterAvis.css"
import { useRouter } from "next/navigation";
import { useDispatch } from 'react-redux';
import { getAvisById } from '@/store/reducers/avis';
const ModifierAvis = ({params}) => {
   // const navigate = useNavigate()
    const dispatch = useDispatch()
    const router = useRouter()
    // Regex pour les validations
    const emailRegex = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
    const commentaireRegex = /^[a-zA-Z0-9\s!@#$%^&*(),.?":{}|<>]+$/
    const nomRegex = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/
    //const { id } = useParams()
    const [state, setState] = useState({
        prenom: '',
        nom: '',
        email: '',
        text: ''

    })
    const [errors, setErrors] = useState({
        prenom: '',
        nom: '',
        email: '',
        text: ''
    })
    function getAvisId() {
        dispatch(getAvisById(params.id))
            .then(res => {
                setState(res.data.data);
                console.log('resultat', res.data.data);
            })
            .catch(err => {
                console.log('Erreur', err);
            })
    }

    function validateField(field, value) {
        switch (field) {
            case 'prenom':
            case 'nom':
                if (!nomRegex.test(value)) setErrors(prev => ({ ...prev, [field]: `${field} n'est pas valide` }))
                else setErrors(prev => ({ ...prev, [field]: '' }))
                break
            case 'email':
                if (!emailRegex.test(value)) setErrors(prev => ({ ...prev, [field]: `Email n'est pas valide` }))
                else setErrors(prev => ({ ...prev, [field]: '' }))
                break
            case 'text':
                if (!commentaireRegex.test(value)) setErrors(prev => ({ ...prev, [field]: `${field} n'est pas valide` }))
                else setErrors(prev => ({ ...prev, [field]: '' }))
                break
            default:
                break
        }
    }
    function HandOnChange(e) {
        const { name, value } = e.target
        validateField(name, value)
        setState(prev => ({ ...prev, [name]: value }))
    }
    // Verifier si un champ a une erreur pour l'afficher
    function fieldHasError(field) {
        if (errors[field] && errors[field] !== '') return true
        return false

    }
    function isValide() {
        if (nomRegex.test(state.nom) && nomRegex.test(state.prenom) && emailRegex.test(state.email) && commentaireRegex.test(state.text)) return true
        else {
            Object.keys(state).forEach(field => {
                validateField(field, state[field])
            })
            return false
        }
    }


    const handleSubmit = (e) => {
        e.preventDefault();
        if (params) {
            isValide() && axios.put(`http://localhost:5000/avis/${params.id}`, state)
                .then(res => {
                    console.log('ya ca ', res.data.data)
                   // window.location.href = '/ajouteravis'
                    //navigate('/AvisListe')
                    router.push('/avisliste')
                    alert('Avis est mis a jour')
                })
                .catch(err => { console.log("Erreur ajout", err) })
        }
        else {
            isValide() && axios.post('http://localhost:5000/avis', state)
                .then(res => {  router.push('/avisliste')
            alert('Avis est ajouter') })
                .catch(err => { console.log("Erreur ajout", err) })
        }

    }






    useEffect(() => {
        if (params) getAvisId()
    }, [params])

    return (
        <div>
            <section id="contact">
                <h2>{!params ? 'Ajouter un Avis' : 'Modifier Avis'}</h2>

                <form onSubmit={handleSubmit}>
                    <label>
                        Nom:
                    </label>
                    <input className={`form-control ${fieldHasError('nom') && "is-invalid"}`} type="text" value={state.nom} onChange={HandOnChange} name='nom' id='nom' placeholder='Entrer le nom' />
                    <div className={fieldHasError('nom') ? "invalid-feedback" : "valid-feedback"}>{errors.nom}</div>

                    <br />
                    <label>
                        Prénom:
                    </label>
                    <input className={`form-control ${fieldHasError('prenom') && "is-invalid"}`} type="text" value={state.prenom} onChange={HandOnChange} name='prenom' id='prenom' placeholder='Entrer le prenom' />
                    <div className={fieldHasError('prenom') ? "invalid-feedback" : "valid-feedback"}>{errors.prenom}</div>

                    <br />
                    <label>
                        Email:
                    </label>
                    <input className={`form-control ${fieldHasError('email') && "is-invalid"}`} type="email" value={state.email} onChange={HandOnChange} name='email' id='email' placeholder='Entrer votre email' />
                    <div className={fieldHasError('email') ? "invalid-feedback" : "valid-feedback"}>{errors.email}</div>
                    <br />
                    <label>
                        Avis:
                    </label>
                    <textarea className={`form-control ${fieldHasError('text') && "is-invalid"}`} value={state.text} onChange={HandOnChange} name='text' id='text' placeholder='Entrer le avis' />
                    <div className={fieldHasError('text') ? "invalid-feedback" : "valid-feedback"}>{errors.text}</div>

                    <br />
                    <button className='btnEnvoyer' type="submit">{!params ? 'Ajouter' : 'Modifier'}</button>
                </form>
            </section>
        </div >
    );
};

export default ModifierAvis
