'use client'
import Link from 'next/link';
import"./Header.css"


function Header() {

    return (
      <header className='header'>
        <nav className='nav'>
          <ul className='list'>
            <li className='item'>
              <Link href='/home'><i className="uil uil-estate"></i>Home</Link>
            </li>
            <li className='item'>
              <Link href='/about'><i className="uil uil-user"></i>About</Link>
            </li>
            <li className='item'>
              <Link href='/projets'><i className="uil uil-briefcase"></i>Project</Link>
            </li>
            <li className='item'>
              <Link href='/contact'><i className="uil uil-navigator"></i>Contact</Link>
            </li>
            <li className='item'>
              <Link href='/avisliste'><i className="uil uil-navigator"></i>Avis</Link>
            </li>
          </ul>
         
        </nav>
      </header>
    );
  }
  
  export default Header