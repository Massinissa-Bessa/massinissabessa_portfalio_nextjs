import './globals.css'
import { Inter } from 'next/font/google'
import Header from './header/page'
import Footer from './footer/page'
import StoreProvider from '@/components/StoreProvider'
const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  title: 'Massinissa Bessa',
  description: 'Portfalio',
}

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <StoreProvider>
        <Header></Header>
        <main>{children}</main>
        <Footer></Footer>
        </StoreProvider>
        </body>
    </html>
  )
}
