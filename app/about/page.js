'use client'
import React, { useState } from 'react';
import { FaAward, FaBriefcase } from 'react-icons/fa';
import './About.css';

function About() {
  const [activeIndex, setActiveIndex] = useState(null);

  const handleItemClick = (index) => {
    setActiveIndex(activeIndex === index ? null : index);
  };

  return (
    <section className='about'>
      <h2 className='title'>About Me
      <svg xmlns="http://www.w3.org/2000/svg"  width="30" height="30" viewBox="0 0 64 64">
  <path d="M32 3 L3 19 L3 45 L32 61 L61 45 L61 19 Z" fill="#000" />
  <path d="M10 20 L32 37 L54 20" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
  <path d="M18 38 L32 46 L46 38" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
</svg>
</h2>
      <span className='dicription'>
        <p>
          Je suis étudiant en informatique. J'ai un esprit d'équipe et j'aime la programmation avec différents langages.
          Mon langage préféré est le C# et j'apprécie également Java. Mon parcours étudiant m'a permis d'acquérir une expérience approfondie dans ce domaine.
        </p>
      </span>
      <div className='aboutData'>
        <div className={`gridData ${activeIndex === 0 ? 'active' : ''}`} onClick={() => handleItemClick(0)}>
          <FaAward className='icon' />
          <h3 className='gridtitle'>FORMATION ACADÉMIQUE</h3>
          <p>_________________________</p>
          <svg
  xmlns="http://www.w3.org/2000/svg"
  viewBox="0 0 24 24"
  fill="none"
  stroke="currentColor"
  stroke-width="1"
  width="24"
  height="24"
  stroke-linecap="round"
  stroke-linejoin="round"
  class="icon"
>
  <path d="M12 5v13M5 12l7 7 7-7" />
</svg>
          <span className='subtitlr'>
            <p>2021 - 2023</p>
            <h4>Diplôme d'études collégiales en Programmation Informatique</h4>
            <p>Collège La Cité, Ottawa</p>
            <p>_________________________</p>
    

            <p>2019 - 2021</p>
            <h4>Baccalauréat (Licence) en Mathématique Informatique</h4>
            <p>Université USTHB, Algérie</p>
            <p>_________________________</p>
            <p>2018 - 2019</p>
            <h4>Bac En mathématiques techniques</h4>
            <p>Lycée frère Hanouti, Algérie</p>
          </span>
        </div>
        <div className={`gridData ${activeIndex === 1 ? 'active' : ''}`} onClick={() => handleItemClick(1)}>
          <FaBriefcase className='icon' />
          <h3 className='gridtitle'>EXPÉRIENCE PROFESSIONNELLE</h3>
          <p>_________________________</p>
          <svg
  xmlns="http://www.w3.org/2000/svg"
  viewBox="0 0 24 24"
  fill="none"
  stroke="currentColor"
  stroke-width="1"
  width="24"
  height="24"
  stroke-linecap="round"
  stroke-linejoin="round"
  class="icon"
></svg>

          <span className='subtitlr'>
            <h4>Développement d'une plateforme Web (Projet scolaire)</h4>
            <p>Collège La Cité, Ottawa</p>
            <p>_________________________</p>
            <h4>Service à la clientèle et soutien informatique</h4>
            <p>Société Mobile, Tizi Ouzou, Algérie</p>
          </span>
        </div>
        <div className={`gridData ${activeIndex === 2 ? 'active' : ''}`} onClick={() => handleItemClick(2)}>
          <FaBriefcase className='icon' />
          <h3 className='gridtitle'>CHAMPS DE COMPÉTENCE</h3>
          <p>_________________________</p>
          <svg
  xmlns="http://www.w3.org/2000/svg"
  viewBox="0 0 24 24"
  fill="none"
  stroke="currentColor"
  stroke-width="1"
  width="24"
  height="24"
  stroke-linecap="round"
  stroke-linejoin="round"
  class="icon"
></svg>
      <span className='subtitlr'>
            <h4>Connaissances de la suite Microsoft office</h4>
            <p>_________________________</p>
            <h4>Maîtrise des systèmes d’exploitation Unix et Windows</h4>
            <p>_________________________</p>
            <h4>Langages de programmation : C#, Java, C++, Python</h4>
            <p>_________________________</p>
            <h4>Programmation WEB: HTML, CSS, PHP, JAVASCRIPT, NodeJS, ReactJS,VueJs</h4>
            <p>_________________________</p>
            <h4>Bonne connaissance des bases de données : MySQL, MongoDB, Cassandra, SQL Server</h4>
          </span>
        </div>
      </div>
    </section>
  );
}

export default About;
