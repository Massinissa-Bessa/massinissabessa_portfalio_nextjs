'use client'
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import './AvisListe.css';
import { FaEdit, FaTrash } from 'react-icons/fa';
import { useDispatch } from 'react-redux';
import { getAllAvis } from '@/store/reducers/avis';

const Avatar = ({ initials }) => {
  return (
    <div className="avatar">
      <span>{initials}</span>
    </div>
  );
};

const AvisListe = () => {
  const dispatch = useDispatch();
  const [avis, setAvis] = useState([]);

  //fonction pour obtenir tous les avis 
  function getAvisListe() {
    dispatch(getAllAvis())
      .then((res) => {
        setAvis(res.data.data)
      })
      .catch(err => console.log('Erreur', err))
  }

  function supprimerAvis(email) {
    axios.delete(`http://localhost:5000/avis/${email}`)
      .then(res => {
        console.log('est suprimer ', res.data)
        alert('Avis supprimer')
        getAvisListe()
       
      })
      .catch(err => {
        console.log('erreur', err)
      })
  }

  function goToAjouterAvis(email=null) {
    window.location.href = `/ajouteravis/${email}`
  }

  function gotoModifier(email = null) {
    window.location.href = `/ajouteravis/${email}`
  }

  useEffect(() => {
    getAvisListe()
  }, [])

  const generateInitials = (prenom, nom) => {
    const prenomInitial = prenom.charAt(0).toUpperCase();
    const nomInitial = nom.charAt(0).toUpperCase();
    return prenomInitial + nomInitial;
  };

  return (
    <section className="avis">
      <div>
        <h2>Avis des visiteurs</h2>
        <table>
          <thead>
            <tr>
              <th>initials</th>
              <th>Email</th>
              <th>Texte</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {avis && avis.map((avis) => (
              <tr key={avis.email}>
                <td>
                  <Avatar initials={generateInitials(avis.prenom, avis.nom)} />
                </td>
                <td className='avisemail'>{avis.email}</td>
                <td>{avis.text}</td>
                <td>
                  <button className="btn btn-success" onClick={() => supprimerAvis(avis.email)}>
                    Supprimer <FaTrash />
                  </button>
                  <button className="btn btnModifier" onClick={() => gotoModifier(avis.email)}>
                    Modifier <FaEdit />
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <button className='btnEnvoyer' onClick={()=> goToAjouterAvis('')}>
        Ajouter un Avis
      </button>
    </section>
  );
}

export default AvisListe;
