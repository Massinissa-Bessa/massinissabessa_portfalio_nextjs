'use client'
import React, { useState, useEffect } from 'react';
import './Projet12.css';
import { FaNodeJs, FaReact, FaDatabase, FaCode } from 'react-icons/fa';


function Projet12({params}) {
    const photos = ['/Capture décran 2023-06-20 031310.png', '/Collageg1.png', '/3.png']
    const photosProjet2 = ['/p21.png', '/p23.png', '/922.png']
    const [currentPhotoP1Index, setCurrentPhotoP1Index] = useState(0);
    const [currentPhotoP2Index, setCurrentPhotoP2Index] = useState(0);
    useEffect(() => {
        const interval = setInterval(() => {
            setCurrentPhotoP1Index((prevIndex) =>
                prevIndex === photos.length - 1 ? 0 : prevIndex + 1

            )
            setCurrentPhotoP2Index((prevIndex) =>
                prevIndex === photosProjet2.length - 1 ? 0 : prevIndex + 1
            )

        }, 5000);

        return () => {
            clearInterval(interval);
        };
    }, [3]);



    // Fonctionnalité : Affichage des photos
    const renderPhotos = () => {
        return (
            <div className="photo-container">
                {photos.map((photo, index) => (
                    <img
                        key={index}
                        src={photo}
                        alt="Photo du projet"
                        className={`project-photo ${index === currentPhotoP1Index ? 'active' : ''}`}
                    />
                ))}
            </div>
        );
    };
    const renderImageProjet2 = () => {
        return (
            <div className="photo-container">
                {photosProjet2.map((photo, index) => (
                    <img
                        key={index}
                        src={photo}
                        alt="Photo du projet"
                        className={`project-photo ${index === currentPhotoP2Index ? 'active' : ''}`}
                    />
                ))}
            </div>
        );
    };

    return (
        <div className="projetData">
            {params.id === '1' ? (
                <>
                    <h1 className="titleProjet">Développement d'une plateforme Web (Projet scolaire)</h1>
                    <p>
                        En tant que réalisateur de ce site web innovant, j'ai eu l'idée de créer une plateforme en ligne mettant en relation les voyageurs se rendant à l'étranger avec les personnes souhaitant expédier des objets. Conscients des obstacles et des coûts élevés associés à l'envoi international, nous voulions proposer une solution pratique et économique. Notre site permet aux utilisateurs de trouver des voyageurs de confiance, de négocier les détails de la livraison et de suivre leurs colis en temps réel. Nous sommes déterminés à rendre le processus d'envoi plus simple, en optimisant les ressources existantes et en offrant une alternative conviviale aux services d'expédition traditionnels. Notre objectif est de transformer la façon dont les gens envoient des objets à l'étranger, en offrant une expérience personnalisée, abordable et fiable
                    </p>
                    <h2 className="langages">Langages utilisés :</h2>
                    <ul>

                        <li>NodeJs<FaNodeJs /></li>
                        <li>React <FaReact></FaReact></li>
                        <li>MySQL<FaDatabase /></li>

                    </ul>
                    <h2 className="img">Photos :</h2>
                    {renderPhotos()}
                </>
            ) : params.id === '2' ? (
                <>
                    <h1 className="titleProjet">Réalisation d'une application WPF Visual Studio</h1>
                    <p>
                        Notre projet consiste en la réalisation d'une application WPF avec Visual Studio,
                        axée sur la gestion des heures de travail et de la paie des employés. Cette application a pour objectif de simplifier le processus de calcul des heures travaillées et des salaires,
                        en automatisant les tâches administratives liées à la gestion des employés.
                        Avec notre application, les employés pourront facilement enregistrer leurs heures de travail quotidiennes, spécifier les tâches effectuées et obtenir un suivi détaillé de leurs activités. L'application permettra également de calculer automatiquement les heures supplémentaires,
                        les congés payés et autres éléments relatifs à la paie.
                    </p>
                    <h2 className="langages">Langages utilisés :</h2>
                    <ul>

                        <li>C#<FaCode /></li>
                        <li>SQL Server<FaDatabase /></li>


                    </ul>

                    <h2 className="img">Photos :</h2>
                    {renderImageProjet2()}
                </>
            ) : (
                <p>Projet non trouvé</p>
            )}
        </div>
    );
}

export default Projet12;