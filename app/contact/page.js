
'use client'
import React, { useState } from 'react'
import "./Contact.css"

function Contact() {
    const handleSubmit = (e) => {
      e.preventDefault();
    }
    function contactMsg(){
      alert('Votre message est Envoyer !')
    }
    const emailRegex = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
    const commentaireRegex = /^[a-zA-Z0-9\s!@#$%^&*(),.?":{}|<>]+$/
    const nomRegex = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/
    const [errors, setErrors] = useState({
      nom: '',
      email: '',
      text: ''
  })
  const [state,setState] = useState({
    nom: '',
    email: '',
    text: ''

})

  function validateField(field, value) {
    switch (field) {
      case 'nom':
      case 'prenom':
        if (!nomRegex.test(value)) {
          setErrors(prev => ({ ...prev, [field]: `${field} n'est pas valide` }))
        } else {
          setErrors(prev => ({ ...prev, [field]: '' }))
        }
        break
        case 'text':
          if(!commentaireRegex.test(value)){
            setErrors(prev =>({...prev, [field]: "Le Text n'est pas Valide"}))
          }else {
            setErrors(prev => ({ ...prev, [field]: '' }))
          }
          break
      case 'email':
        if (!emailRegex.test(value)) {
          setErrors(prev => ({ ...prev, [field]: 'Email n\'est pas valide' }))
        } else {
          setErrors(prev => ({ ...prev, [field]: '' }))
        }
        break
      default:
        break
    }
}
function HandOnChange(e) {
  const { name, value } = e.target
  validateField(name, value)
  setState(prev => ({ ...prev, [name]: value }))
}
function fieldHasError(field) {
  if (errors[field] && errors[field] !== '') return true
  return false

}
function isValide() {
  if (nomRegex.test(state.nom)  && emailRegex.test(state.email) && commentaireRegex.test(state.text)) {
    alert('Votre message est Envoyer !')
    return true
  }
  else {
      Object.keys(state).forEach(field => {
          validateField(field, state[field])
      })
      return false
  }
}
  
    return (
      <div>
        <section id="contact">
        <h2>Contact</h2>
        <form onSubmit={handleSubmit}>
          <label htmlFor="nom">Nom : </label>
          <input className={`form-control ${fieldHasError('nom') && "is-invalid"}`}  type="text" id="nom" value={state.nom} name='nom'  placeholder='Entrer le nom' onChange={HandOnChange}/>
          <div className={fieldHasError('nom') ? "invalid-feedback" : "valid-feedback"}>{errors.nom}</div>

          <label htmlFor="email">Email :</label>
          <input className={`form-control ${fieldHasError('email') && "is-invalid"}`}  type="email" id="email" value={state.email} name='email' onChange={HandOnChange} placeholder='Entrer lemal' />
          <div className={fieldHasError('email') ? "invalid-feedback" : "valid-feedback"}>{errors.email}</div>

          <label htmlFor="message">Message :</label>
          <textarea className={`form-control ${fieldHasError('text') && "is-invalid"}`}  id="message"  value={state.text} name='text' onChange={HandOnChange} placeholder='Entrer le text'/>
          <div className={fieldHasError('text') ? "invalid-feedback" : "valid-feedback"}>{errors.text}</div>

          <button type="submit"className='btnEnvoyer' onClick={isValide}>Envoyer
          <svg
                                class="button__icon"
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                            >
                                <path
                                    d="M14.2199 21.9352C13.0399 21.9352 11.3699 21.1052 10.0499 17.1352L9.32988 14.9752L7.16988 14.2552C3.20988 12.9352 2.37988 11.2652 2.37988 10.0852C2.37988 8.91525 3.20988 7.23525 7.16988 5.90525L15.6599 3.07525C17.7799 2.36525 19.5499 2.57525 20.6399 3.65525C21.7299 4.73525 21.9399 6.51525 21.2299 8.63525L18.3999 17.1252C17.0699 21.1052 15.3999 21.9352 14.2199 21.9352ZM7.63988 7.33525C4.85988 8.26525 3.86988 9.36525 3.86988 10.0852C3.86988 10.8052 4.85988 11.9052 7.63988 12.8252L10.1599 13.6652C10.3799 13.7352 10.5599 13.9152 10.6299 14.1352L11.4699 16.6552C12.3899 19.4352 13.4999 20.4252 14.2199 20.4252C14.9399 20.4252 16.0399 19.4352 16.9699 16.6552L19.7999 8.16525C20.3099 6.62525 20.2199 5.36525 19.5699 4.71525C18.9199 4.06525 17.6599 3.98525 16.1299 4.49525L7.63988 7.33525Z"
                                    fill="#fff"
                                ></path>
                                <path
                                    d="M10.11 14.7052C9.92005 14.7052 9.73005 14.6352 9.58005 14.4852C9.29005 14.1952 9.29005 13.7152 9.58005 13.4252L13.16 9.83518C13.45 9.54518 13.93 9.54518 14.22 9.83518C14.51 10.1252 14.51 10.6052 14.22 10.8952L10.64 14.4852C10.5 14.6352 10.3 14.7052 10.11 14.7052Z"
                                    fill="#fff"
                                ></path>
                            </svg></button>
        </form>
        </section>
       
      </div>
    );
  }
  
  export default Contact;