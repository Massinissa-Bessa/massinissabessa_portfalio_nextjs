import React from 'react';
import { FaExternalLinkAlt } from 'react-icons/fa';
import Image from 'next/image';
import Link from 'next/link';
import './Projets.css';


const Avatar = ({ initials }) => {
  return (
    <div className={styles.avatar}>
      <span>{initials}</span>
    </div>
  );
};

const Projet = ({ titre, description, imageUrl, lienProjet }) => {
  const generateInitials = (titre) => {
    return titre.charAt(0).toUpperCase();
  };

  return (
    <section className='projet'>
      <div className='projet-card'>
        <h3 className='projet-titre'>{titre}</h3>
        <div className='projet-image'>
          <Image src={imageUrl} alt={titre} width={400} height={300} />
        </div>
        <p className='projet-description'>{description}</p>
        <div className='projet-lien'>
          <Link href={lienProjet}>
            Voir le projet <FaExternalLinkAlt />
          </Link>
        </div>
      </div>
    </section>
  );
};

const Projets = () => {
  const projets = [
    {
      titre: 'Projet 1',
      description: 'Développement dune plateforme Web (Projet scolaire)',
      imageUrl: '/image1.png',
      lienProjet: '/projet12/1',
    },
    {
      titre: 'Projet 2',
      description: 'Réalisation d\'une application WPF Visual Studio',
      imageUrl: '/employe.png',
      lienProjet: '/projet12/2',
    },
    // Ajoutez d'autres projets ici
  ];

  return (
    <div className='projets-container'>
      <h2 className='projets-titre'>Mes Projets
        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 64 64">
          <circle cx="32" cy="32" r="30" fill="#000" />
          <path d="M32 20 L32 44" stroke="#FFFFFF" strokeWidth="2" strokeLinecap="round" />
          <path d="M20 32 L44 32" stroke="#FFFFFF" strokeWidth="2" strokeLinecap="round" />
        </svg>
      </h2>
      <div className='projets-liste'>
        {projets.map((projet, index) => (
          <Projet
            key={index}
            titre={projet.titre}
            description={projet.description}
            imageUrl={projet.imageUrl}
            lienProjet={projet.lienProjet}
          />
        ))}
      </div>
    </div>
  );
};

export default Projets;
