import axios from 'axios'

// Creation d'une instance d'axios pour ne pas toujours taper url de base

const apiRequest = axios.create({
    baseURL:'http://localhost:5000/' 
})

export default apiRequest

