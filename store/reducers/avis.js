import apiRequest from "../axios/api"

const LIST_AVIS = 'LIST_AVIS'
const CURRENT_AVIS = 'CURRENT_AVIS'

export const setAllAvis = (avis) => ({ type: LIST_AVIS, payload: avis })
export const setAvis = avis => ({ type: CURRENT_AVIS, payload: avis })

//afficher les avis avec le id
export const getAvisById = (email) => async (dispatch) => {
    const result = await apiRequest({
        method: 'get',
        url: `/avis/${email}`

    })
    dispatch(setAvis(result.data.data))
    return result
}

//Afficher tout les avis
export const getAllAvis = () => async (dispatch) => {

    const result = await apiRequest({
        method: 'get',
        url: '/avis',

    })
    dispatch(setAllAvis(result.data.data))
    return result
}


const initialState = {
    allAvis: [],
    avis: {}
}
//fonction globale
export const avisReducer = (state = initialState, action) => {
    const { type, payload } = action
    switch (type) {
        case LIST_AVIS:
            return { ...state, allAvis: payload }
        case CURRENT_AVIS:
            return { ...state, avis: payload }
        default:
            return state

    }
}