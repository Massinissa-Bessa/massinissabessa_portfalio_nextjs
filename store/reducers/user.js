//les action  de notre store pour utilisateur 
const ADD_USER = 'ADD_USER'
const GET_USER = 'GET_USER'
const GET_USERS = 'GET_USERS'
const DELETE_USER = 'DELETE_USER'
const UPDATE_USER = 'UPDATE_USER'




export const addUser = (user) => ({ type: ADD_USER, payload: user })
export const getUser = (id) => ({ type: GET_USER, payload: id })
export const getUsers = () => ({ type: GET_USERS })  //pas necessaire le plus souvent 
export const deletUser = (id) => ({ type: DELETE_USER, payload: id })
export const updateUser = (id, user) => ({ type: UPDATE_USER, payload: { id, user } })

//Reducteur pour les utilisateur
//Etat initial de la variable state (variable locale glbale)

const initialState = {
    allUsers: [],
    currentUser: {}
}
export const userReducer = (state = initialState, action) => {

    const { type, payload } = action
    switch (type) {
        case ADD_USER:
            return {
                ...state, allUsers: [...state.allUsers, payload]
            }
        case GET_USER:
            return {
                ...state, currentUser: state.find(user => user.id === payload)
            }
        case DELETE_USER:
            return {
                ...state, allUsers: state.allUsers.filter(user => user.id !== payload)
            }
        case DELETE_USER:
            return {
                ...state, allUsers: state.allUsers.map(user => {
                    if (user.id === payload.id)
                        return payload.user
                    return user
                })
            }
            default:
                return state
    }
}




