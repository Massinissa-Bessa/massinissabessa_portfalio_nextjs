import { configureStore } from "@reduxjs/toolkit";
import reducers from "../store/reducers/index";


const store=configureStore({
    reducer:reducers

})
export default store